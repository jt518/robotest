# Computer Vision Guided Testing

For prototypes related to testing modelbuilder using CV to locate GUI features.

On turtleland4, uses ~/.py3-venv/cvgt
(cvgt = computer vision guided test)

To run ace3p example:

* create & activate python env
* create .env file with 2 variables:
  ** MODELBUILDER=full-path-to-modelbuilder-executable
  ** LAYOUT_SPEC=relative-path-to-layout-spec-file (e.g., ace3p.yml)
* can also add optional
  ** DEBUG=1
  ** XOFFSET=<int>  to adjust screen position reported by xdotool
  ** YOFFSET=<int>
* > python run_ace3p.py

For example
```
MODELBUILDER=full-path-to-modelbuilder-executable
LAYOUT_SPEC=relative-path-to-layout-spec-file (e.g., ace3p.yml)
DEBUG=1
XOFFSET=10
YOFFSET=-45
```
