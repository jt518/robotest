

class Window:
    """Represents one window (rectangle) in a working image (screenshot)."""
    def __init__(self, name, position=[0, 0], size=[None, None]):
        self.name = name
        self.position = position  # relative position with respect to parent window [x, y]
        self.size = size          # window [width, height]

        self.children = list()
        self.layout = None    # container for children Window instances

    def bounds(self):
        """Returns tuple (left, upper, right, lower) coordinates."""
        x, y = self.position
        w, h = self.size
        return (x, y, x + w, y + h)

    def build_window_dict(self, d):
        d[self.name] = self
        for child in self.children:
            child.build_window_dict(d)

    def to_dict(self):
        d = dict(
            name=self.name,
            position=self.position,
            size = self.size,
            layout = self.layout
            )
        if self.children:
            l = list()
            for child in self.children:
                d_child = child.to_dict()
                l.append(d_child)
            d['children'] = l

        return d
