import time

import pyautogui

x = 1436
y = 482
w = 970
h = 700

xmouse = x + w - 100
ymouse = y + 10
pyautogui.moveTo(xmouse, ymouse)
pyautogui.click()

pyautogui.keyDown('alt')
pyautogui.write('f')
pyautogui.keyUp('alt')

time.sleep(1.5)
pyautogui.press('esc')

time.sleep(1.0)
pyautogui.moveTo(xmouse, ymouse)
pyautogui.click()
pyautogui.keyDown('ctrl')
pyautogui.write('q')
pyautogui.keyUp('ctrl')
