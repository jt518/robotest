"""Run modelbuilder from python.

Hard coded for turtleland4 desktop
"""

import os
import subprocess
import time

from PIL import Image, ImageGrab
import pyautogui

print('pyautogui.size():', pyautogui.size())

# turtleland4:
# exe_path = '/home/john/temp/nightly/211008/modelbuilder-21.07.0-Linux-x86_64/bin/modelbuilder'
# turtleland:
exe_path = '/home/john/temp/production/nightly/modelbuilder-ace3p-centos7-211106/bin/modelbuilder'

source_path = os.path.abspath(os.path.dirname(__file__))

mb_proc = subprocess.Popen([exe_path, '--dr'])
mb_pid = mb_proc.pid
print('modelbuilder pid:', mb_pid)

# Wait for modelbuilder window to open
print('Waiting for modelbuilder window to open')
handle = ''
while not bool(handle):
    time.sleep(1.0)
    handle = subprocess.getoutput('xdotool search --pid {}'.format(mb_proc.pid))
    # print('xdotool handle: {} ({})'.format(handle, (bool(handle))))

output = subprocess.getoutput('xdotool windowfocus --sync {}'.format(handle))
print('focus output', output)

geometry = subprocess.getoutput('xdotool getwindowgeometry {}'.format(handle))
print('geometry:', geometry)
geom_list = geometry.split('\n')

position_line = geom_list[1]
position_list = position_line.split()
position = position_list[1]
coords = position.split(',')
x = int(coords[0])
y = int(coords[1])
print('x,y:', x, y)
y -= 30 # there is an offset for some reason (title bar?)

size_line = geom_list[2]
size_list = size_line.split()
size = size_list[1]
width, height = size.split('x')
width = int(width)
height = int(height)
print('w,h:', width, height)

# im1 = pyautogui.screenshot('im1.png', (x, y, width, height))
im1 = ImageGrab.grab(bbox=(x, y, x+width, y+height))
print('im1', im1)
print('im1:', im1.format, im1.size, im1.mode)
path = os.path.join(source_path, 'im1.png')
im1.save(path)
print('Wrote', path)

xmouse = x + 20
ymouse = y + 0
pyautogui.moveTo(xmouse, ymouse)
# pyautogui.click()

# pyautogui.keyDown('alt')
# pyautogui.write('f')
# pyautogui.keyUp('alt')

time.sleep(0.5)
# im2 = pyautogui.screenshot('im2.png', (x, y, 200, 400))
im2 = ImageGrab.grab(bbox=(x, y, x+width, y+30))
print('im2', im2)
print('im2:', im2.format, im2.size, im2.mode)
path = os.path.join(source_path, 'im2.png')
im2.save(path)
print('Wrote', path)

pyautogui.press('esc')

print('Shutting down')
time.sleep(1.5)
pyautogui.keyDown('ctrl')
pyautogui.write('q')
pyautogui.keyUp('ctrl')

mb_proc.wait()
print('finis')
