"""Compare two images for differences."""

import argparse
# import math
import os

import numpy as np
import cv2
# from PIL import Image

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('reference_image')
    parser.add_argument('test_image')
    args = parser.parse_args()

    # img1 = cv2.imread(args.reference_image)
    # ref_img = cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY)
    # img2 = cv2.imread(args.test_image)
    # test_img = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    img1 = cv2.imread(args.reference_image, cv2.IMREAD_GRAYSCALE)
    img2 = cv2.imread(args.test_image, cv2.IMREAD_GRAYSCALE)

    diff_img = cv2.subtract(img1, img2)

    cv2.imshow('DIFF', diff_img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
