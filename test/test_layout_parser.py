import os
import unittest

import yaml

import robotest
from robotest.layout_parser import LayoutParser

class TestRegion(unittest.TestCase):
    def setUp(self):
        self.source_dir = os.path.abspath(os.path.dirname(__file__))

    def test_parser(self):
        yml_path = os.path.join(self.source_dir, 'basic_layout.yml')
        spec = None
        with open(yml_path) as instream:
            spec = yaml.safe_load(instream)
        assert spec is not None, 'Failed to open spec file {}'.format(yml_path)
        # print('data:', spec)

        parser = LayoutParser()
        window_list = parser.parse(spec)
        assert len(window_list) == 1, \
            'expected window_list to have 1 item not {}'.format(len(window_list))

        root_window = window_list[0]

        # Test regions
        test_regions = [
            ('root', 0, 0, 640, 480),
            ('left', 0, 0, 300, 480),
            ('right', 300, 0, 640, 480),
            ('right_upper', 300, 0, 640, 200),
            ('right_lower', 300, 200, 640, 480),
        ]

        # Apply screen offset
        sc = [222, 333]
        root_window.set_screen_position(sc)

        for t in test_regions:
            name, x0, y0, x1, y1 = t
            region = root_window.region(name)
            assert region is not None, 'missing region "{}"'.format(name)

            expect_bbox = (x0 + sc[0], y0 + sc[1], x1 + sc[0], y1 + sc[1])
            bbox = region.screen_bbox()
            assert bbox == expect_bbox, \
                'screen_rect mismatch for "{}": rect {} should be {}'.format(region.name, bbox, expect_bbox)


if __name__ == '__main__':
    unittest.main()
