"""Window geometry."""

from .region import Region, RegionType

class Window(Region):
    def __init__(self, name, position=[0, 0], size=[None, None]):
        super(Window, self).__init__(name, position, size)

    def type(self):
        return RegionType.Window
