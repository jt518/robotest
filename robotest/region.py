"""Basic UI feature."""

class RegionType:
    Region = 'region'
    Field = 'field'
    Menu = 'menu'
    Window = 'window'


class Region:
    """Represents one rectangle in a graphic user interface (screenshot)."""
    def __init__(self, name, position=[0, 0], size=[None, None]):
        self.name = name
        self.position = position     # position within the parent window [x, y]
        self.size = size             # window [width, height]

          # Origin in screen coordinates
        self.screen_offset = [0, 0]

        # Children regions
        self.children = list()
        self.region_lookup = dict()  # <name, Region>

    def type(self):
        return RegionType.Region

    def build_region_lookup(self):
        self.region_lookup.clear()
        self._build_region_dict(self.region_lookup)

    def geometry(self, padding = 0):
        """Returns tuple (left, upper, right, lower) coordinates.

        Independent of screen position.
        """
        x0 = self.position[0] - padding
        y0 = self.position[1] - padding
        if padding > 0:
            x0 = 0 if x0 < 0 else x0
            y0 = 0 if y0 < 0 else y0
        x1 = x0 + self.size[0] + padding
        y1 = y0 + self.size[1] + padding

        return (x0, y0, x1, y1)

    def region(self, name):
        """Returns subregion by name."""
        return self.region_lookup.get(name)

    def region_names(self):
        return self.region_lookup.keys()

    def screen_bbox(self):
        """Returns tuple (left, upper, right, lower) coordinates."""
        x = self.screen_offset [0] + self.position[0]
        y = self.screen_offset [1] + self.position[1]
        w, h = self.size
        return (x, y, x + w, y + h)

    def screen_point(self):
        """Returns centroid coordinates."""
        x = self.screen_offset [0] + self.position[0] + self.size[0] // 2
        y = self.screen_offset [1] + self.position[1] + self.size[1] // 2
        return (x, y)

    def set_screen_position(self, coords):
        """"""
        self.screen_offset = coords
        for r in self.children:
            r._set_screen_position(coords)

    def size(self):
        return self.size

    def _build_region_dict(self, d):
        d[self.name] = self
        for child in self.children:
            child._build_region_dict(d)

    def _set_screen_position(self, coords):
        self.screen_offset = coords
        for child in self.children:
            child._set_screen_position(coords)

    def to_dict(self):
        d = dict(
            name=self.name,
            position=self.position,
            screen_offset=self.screen_offset,
            size = self.size,
            children=self.children,
            )
        if self.children:
            l = list()
            for child in self.children:
                d_child = child.to_dict()
                l.append(d_child)
            d['children'] = l

        return d
