"""Parses window specificiation(s)."""

import json
import os
import sys

import yaml

from .region import Region, RegionType
from .field import Field
from .menu import Menu
from .window import Window

class LayoutType:
    HBox = 'hbox'
    VBox = 'vbox'

class LayoutParser:
    def __init__(self):
        self.region_dict = dict()  # <name, Region>

    def parse(self, spec):
        """Reads window specification(s).

        @input spec: either dictionary or list of dictionaries
        @return list of Window instances
        """
        ret_list = list()
        if isinstance(spec, list):
            for window_spec in spec:
                window = self._parse_window(window_spec)
                ret_list .append(window)
        elif isinstance(spec, dict):
            window = self._parse_root_spec(spec)
            ret_list.append(window)

        return ret_list

    def _parse_window(self, spec):
        """Parses spec for window."""
        name = spec.get('name', 'unnamed')
        position = spec.get('position', [0, 0])
        size = spec.get('size')

        window = Window(name, position=position, size=size)
        self.region_dict[name] = window
        self._parse_region(window, spec)
        window.build_region_lookup()
        return window

    def _check_name(self, name):
        if name not in self.region_dict:
            return name

        for i in range(1, 999):
            test_name = '{}-{}'.format(name, i)
            if test_name not in self.region_dict:
                return test_name

    def _new_region(self, spec, position, size):
        """Create new Region instance."""
        name = spec.get('name', 'region')
        name = self._check_name(name)

        type = spec.get('type')
        if type == 'menu':
            region = Menu(name, position, size)
        elif type == 'field':
            region = Field(name, position, size)
        else:
            region = Region(name, position, size)

        action_offset = spec.get('action_offset')
        if action_offset is not None:
            region.action_offset = action_offset

        return region

    def _parse_hbox(self, parent_region, parent_spec):
        """"""
        x, y = parent_region.position
        parent_width, parent_height = parent_region.size
        parent_region.layout = LayoutType.HBox
        parent_region.children = list()
        last = len(parent_spec) - 1
        remaining_width = parent_width
        for i, spec in enumerate(parent_spec):
            if i == last:
                width = remaining_width
            else:
                width = spec.get('width')
            assert width is not None, 'Spec for region {} missing width'.format(name)
            size = [width, parent_height]

            region = self._new_region(spec, [x, y], size)
            self.region_dict[region.name] = region
            self._parse_region(region, spec)

            parent_region.children.append(region)
            x += width
            remaining_width -= width

    def _parse_fields(self, parent_region, parent_spec):
        """"""
        for i, spec in enumerate(parent_spec):
            name = spec.get('name')
            assert name is not None, 'Field spec missing name'
            region_type = spec.get('type', 'field')
            height = spec.get('height')
            width = spec.get('width')
            if (region_type == 'menu'):
                assert height is not None, 'Menu item {} missing height in spec'.format(name)
                assert width is not None, 'Menu item {} missing width in spec'.format(name)
            if height is not None:
                height = int(height)
            if width is not None:
                width = int(width)

            spec['type'] = region_type
            region = self._new_region(spec, [0, 0], [width, height])
            self._parse_region(region, spec)
            parent_region.children.append(region)

    def _parse_vbox(self, parent_region, parent_spec):
        """"""
        x, y = parent_region.position
        parent_width, parent_height = parent_region.size
        parent_region.layout = LayoutType.VBox
        parent_region.children = list()
        last = len(parent_spec) - 1
        remaining_height = parent_height
        for i, spec in enumerate(parent_spec):
            name = spec.get('name', 'region')
            if i == last:
                height = remaining_height
            else:
                height = spec.get('height')
            assert height is not None, 'Spec for region {} missing height'.format(name)
            size = [parent_width, height]

            region = self._new_region(spec, [x, y], size)
            self.region_dict[region.name] = region
            self._parse_region(region, spec)
            parent_region.children.append(region)
            y += height
            remaining_height -= height

    def _parse_region(self, region, spec):
        """"""
        if LayoutType.VBox in spec:
            self._parse_vbox(region, spec.get(LayoutType.VBox))
        elif LayoutType.HBox in spec:
            self._parse_hbox(region, spec.get(LayoutType.HBox))
        elif 'fields' in spec:
            self._parse_fields(region, spec.get('fields'))
        region.build_region_lookup()
