"""Vision guided app automation."""

import sys
if sys.platform != 'linux':
    msg = 'Sorry robotest only works on linux, not {}'.format(sys.platform)
    raise NotImplementedError(msg)

import os
import subprocess
import time
from typing import Union

from PIL import Image, ImageDraw, ImageGrab
import pyautogui

from .region import Region, RegionType
from .field import Field
from .window import Window
from .layout_parser import LayoutParser
from .screen_ops_xdotool import ScreenOpsXdotool # specific to unix
from .vision_ops import VisionOps

class Robotest:
    # Configure as poor-mans singleton - can only instantiate once
    instance = None

    def __init__(self, debug_mode: bool = False) -> None:
        assert self.__class__.instance is None, \
            'Second attempt to instantiate Robotest - not allowed'
        self._debug_mode = debug_mode

        # Operation instances
        self._screen_ops = ScreenOpsXdotool(debug_mode)
        screen_size = pyautogui.size()
        self._vision_ops = VisionOps(screen_size, debug_mode)

        # Appication process
        self._proc: subprocess.Popen = None

        # Window handle of main application
        self._mainwindow_handle:str = ''

        # Dictionary of <name, Window>
        self._window_dict: dict = dict()

        # Offset between screen ops and image grab
        # e.g., x_imagegrab = x_screenops + position_offset[0]
        self._position_offset = [0, 0]

    def load_layout(self, spec: dict) -> Union[Window, None]:
        """"""
        self._window_dict.clear()

        parser = LayoutParser()
        window_list = parser.parse(spec)
        # print('window_list', [w.name for w in window_list])
        for window in window_list:
            name = window.name
            self._window_dict[name] = window

    def get_window(self, name: str) -> Union[Window, None]:
        """"""
        return self._window_dict.get(name)

    def start_exe(self, exe_args: list, main_window: Window) -> subprocess.Popen:
        """Launches GUI executable and locates main window geometry.

        Expects executable to display a single window.
        Calls sys.exit(-1) if unable to start or find the window.

        @input exe_args: path to executable and arguments
        @return: (process, x, y, width, height)
        """
        if not os.path.exists(exe_args[0]):
            print('Executable file not found: {}'.format(exe_args[0]))
            sys.exit(-1)

        self._proc = subprocess.Popen(exe_args)
        print('Process pid:', self._proc.pid)

        # Wait for window to get initialized
        time.sleep(1.0)

        # Get window geometry from screen ops
        screen_rect = self._screen_ops.locate_mainwindow(self._proc)
        x, y, w, h = screen_rect

        # Adjust window position based on vision measurements
        self._position_offset = self._vision_ops.locate_mainwindow(screen_rect)
        x_screen = x + self._position_offset[0]
        y_screen = y + self._position_offset[1]
        main_window.set_screen_position([x_screen, y_screen])

        return self._proc

    def grab_dropdown_menu(self, menu):
        """"""
        # Open menu and grab screenshot
        x, y = menu.screen_point()
        pyautogui.moveTo(x, y, 1.0)
        pyautogui.mouseDown()
        # Dwell for image grab
        pyautogui.moveTo(x+10, y, 1.0)
        print('CLICKED MOUSE AT', x, y)

        im = ImageGrab.grab(bbox=menu.menu_screen_bbox())
        filename = '{}.png'.format(menu.name)
        im.save(filename)
        pyautogui.mouseUp()

        # Go back and click again
        # For some reason, this fixes dwell time on next mouse command
        pyautogui.moveTo(x-10, y, 0.1)
        pyautogui.click()
        return im

    def locate_fields(self, parent_region: Region, color_img = None):
        """Find fields located inside a region."""
        if color_img is None:
            color_img = ImageGrab.grab(bbox=parent_region.screen_bbox())
        gray_img = color_img.convert('L')
        filename = 'fields.png'
        gray_img.save(filename)
        print('Wrote', filename)
        d = self._vision_ops.find_text(gray_img)
        for region in parent_region.children:
            t = d.get(region.name)
            if t is None:
                print('Failed to find region', region.name)
                continue
            x = parent_region.position[0] + t[0]
            y = parent_region.position[1] + t[1]
            region.position = [x, y]
            region.size = t[2:]
            region.screen_offset = parent_region.screen_offset

    def locate_menus(self, region: Region):
        """Locates fields in menu opened by clicking the region."""
        im = self.grab_dropdown_menu(region)
        self.locate_fields(region, im)

    def locate_popup_window(self):
        """Find coordinates of window that is not the main window.

        Returns screen geometry (rectangle in screen coords)
        """
        screen_rect = self._screen_ops.locate_popup_window()

        # Adjustments:
        x, y, w, h = screen_rect
        x += self._position_offset[0]
        y += self._position_offset[1]
        h += 40 # for title bar
        return [x, y, w, h]

    def locate_text_field(self, region:Region, text:str, name = None, threshold=None) -> Union[Field, None]:
        """Grabs image of region and finds text, returning Field instance."""
        color_img = ImageGrab.grab(bbox=region.screen_bbox())
        gray_img1 = color_img.convert('L')

        if threshold is None:
            gray_img = gray_img1
        else:
            gray_img = gray_img1.point(lambda p: p > threshold and 255)

        if self._debug_mode:
            filename = 'fields.png'
            gray_img.save(filename)
            print('Wrote', filename)
        d = self._vision_ops.find_text(gray_img)
        if not text in d:
            return None

        # Get position
        x, y, w, h, = d.get(text)
        position = [0] * 2
        position[0] = x + region.position[0]
        position[1] = y + region.position[1]

        # Find or instantiate Field instance
        if name is None:
            name = text
        f = region.region(name)
        if f is None:
            f = Field(name, position=position, size=(w, h))
        elif f.type() not in [RegionType.Field, RegionType.Menu]:
            print('Region {} is not a Field'.format(f.name))
        else:
            f.position = position
            f.size = (w, h)

        f.screen_offset = region.screen_offset
        return f
