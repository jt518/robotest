"""Menu region.

A menu that is display by some UI action
"""

from .region import RegionType
from .field import Field

class Menu(Field):
    """Geometric field containing a horizontal menu bar.

    Children are dropdown regions that are only visible when opened by some UI action.
    As such, the positions of their geometry and submenus must be obtained after
    instantiation.
    """
    def __init__(self, name, position=[0, 0], size=[None, None]):
        super(Menu, self).__init__(name, position, size)
        self.menu_size = [0, 0]

        # Store the size coords in a menu_size variable
        # This is the size of the menu when it is opened
        self.menu_size, self.size = size, [0, 0]

    def type(self):
        return RegionType.Menu

    def menu_screen_bbox(self):
        """Returns where menu opens up to in screen coords."""
        x1, y1, x2, y2 = self.screen_bbox()
        return (x1, y1, x1 + self.menu_size[0], y1 + self.menu_size[1])
