"""Screen operations using xdotool (unix)"""

import subprocess
import sys
import time


class ScreenOpsXdotool:
    def __init__(self, debug_mode: bool = False):
        self._debug_mode = debug_mode

        # Main application's process object
        self._proc: subprocess.Popen = None

        # Application's main window id
        self._mainwindow_id = None

    def locate_mainwindow(self, main_process: subprocess.Popen) -> tuple:
        """Finds first (main) window for this app and returns its screen geometry.

        Stores the process so that it can shut it down in case of error
        Also sets focus to the window.
        Runs xdotool as subprocess.

        @return window geometry (x, y, w, h)
        """
        self._proc = main_process

        # Find the window id
        args = 'xdotool search --pid {} --limit 1 --all --sync'.format(self._proc.pid)
        try:
            result = subprocess.run(args, shell=True, capture_output=True, text=True, timeout=10)
        except subprocess.TimeoutExpired:
            print('Timeout waiting for modelbuilder main window')
            self._proc.kill()
            sys.exit(-1)
        self._check_run_result(result)
        time.sleep(1.0) # give some time for window to display

        self._mainwindow_id = result.stdout.strip()
        if self._debug_mode:
            print('MAIN WINDOW ID', self._mainwindow_id)
            print('Setting window focus')
        args = 'xdotool windowfocus --sync {}'.format(self._mainwindow_id)
        result = subprocess.run(args, shell=True, capture_output=True, text=True)
        self._check_run_result(result)

        # Get window geometry
        args = 'xdotool getwindowgeometry {}'.format(self._mainwindow_id)
        result = subprocess.run(args, shell=True, capture_output=True, text=True)
        self._check_run_result(result)

        geometry = result.stdout
        print('geometry:', geometry)
        geom_list = geometry.split('\n')

        position_line = geom_list[1]
        # print('POSITION_LINE', position_line)
        position_list = position_line.split()
        position = position_list[1]
        coords = position.split(',')
        x = int(coords[0])
        y = int(coords[1])

        size_line = geom_list[2]
        size_list = size_line.split()
        size = size_list[1]
        width, height = size.split('x')
        width = int(width)
        height = int(height)
        rect = x, y, width, height
        if self._debug_mode:
            print('xdotool x,y,w,h:', rect)
        return rect

    def locate_popup_window(self) -> tuple:
        """Find coordinates of window that is not the main window.

        Current logic presumes that only the main window and one other are visible.
        """
        # Give some time for window to get initialized
        time.sleep(1.0)
        args = 'xdotool search --pid {} --onlyvisible'.format(self._proc.pid)
        try:
            result = subprocess.run(args, shell=True, capture_output=True, text=True, timeout=10)
        except subprocess.TimeoutExpired:
            print('Timeout waiting for modelbuilder main window')
            self._proc.kill()
            sys.exit(-1)
        self._check_run_result(result)

        text = result.stdout.strip()
        print('XDOTOOL OUTPUT:', text)
        full_list = text.split('\n')
        assert(len(full_list)) == 2, 'Expected list of 2 windows, not {}'.format(full_list)
        new_list = [id for id in full_list if id != self._mainwindow_id]
        assert(len(new_list)) == 1, 'Expected new list with 1 window, not {}'.format(new_list)
        new_id = new_list[0]
        print('NEW WINDOW', new_id)

        # Get geometry
        args = 'xdotool getwindowgeometry {}'.format(new_id)
        result = subprocess.run(args, shell=True, capture_output=True, text=True)
        self._check_run_result(result)

        geometry = result.stdout
        print('geometry:', geometry)
        geom_list = geometry.split('\n')

        position_line = geom_list[1]
        # print('POSITION_LINE', position_line)
        position_list = position_line.split()
        position = position_list[1]
        coords = position.split(',')
        x = int(coords[0])
        y = int(coords[1])
        print('xdotool x,y:', x, y)

        size_line = geom_list[2]
        size_list = size_line.split()
        size = size_list[1]
        width, height = size.split('x')
        width = int(width)
        height = int(height)
        print('w,h:', width, height)

        return (x, y, width, height)

    def _check_run_result(self, result:subprocess.CompletedProcess, proc:subprocess.Popen=None) -> None:
        """Checks result of subprocess.run() command.

        @input result: output of CompletedProcess object returned by subprocess.run()
        @input proc: process instance to kill if result is not ok
        """
        if result.returncode != 0:
            print('subprocess run failed with command {}'.format(result.args))
            print(result.stdout)
            print(result.stderr)
            if proc is not None:
                proc.kill()
            sys.exit(result.returncode)

